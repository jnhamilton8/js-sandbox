This project contains a number of modules that house small proof of concepts for:
- cypress
- mocha and chai with node.js

And a module containing some of the popular data structures and algorithms coded in JavaScript, used whilst I was 
learning about the same and wanted to practice my js!

To run the Mocha tests:

1. Clone the repo 
2. Run `npm install`
3. Run `npm test`
