

import sayHello from "../src/index.js"


const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const should = chai.should();
chai.config.truncateThreshold = 0; //disable truncating expected and actual properties on failure

describe('Mathematical Operations', function() { //sadly calls to this.timeout will not work with arrow ES6 syntax
    this.timeout(500); //set suite level timeout - will fail if the tests do not execute in this time

    it('Addition of two numbers', (done) => {
        this.timeout(3000)  //test level timeout

        const a = 20;
        const b = 10;

        const c = a + b;

        assert.strictEqual(c, 30); //using chai assert
        assert.equal(c, 30);
        expect(c).to.equal(30, "a and b are not equal"); //using Chai expect
        expect(c).to.be.an('number'); //validate type
        c.should.equal(30); //using the should style

        done(); //tells Mocha the async call is done. If this is not set, this test will fail
    });

    it('Subtraction of two numbers', () => {
        const a = 20;
        const b = 10;

        const c = a - b;

        assert.strictEqual(c, 10);
    });

    it('Multiplication of two numbers', () => {
        const a = 10;
        const b = 10;

        const c = a * b;

        assert.strictEqual(c, 100);
    });

    it('Division of two numbers', () => {
        const a = 50;
        const b = 10;

        const c = a / b;

        assert.strictEqual(c, 5);
    });
});

describe('Mocha Hooks', () => {
    before('Execute before all tests', () => {
        console.log("Execute before all tests");
    });

    beforeEach('Execute before each test', () => {
        console.log("Execute before each test");
    });

    after('Execute after all tests', () => {
        console.log("Execute after all tests");
    });

    afterEach('Execute after each test', () => {
        console.log("Execute after each test");
    });

    it('Mocha hooks test', () => {
        console.log("Mocha - This is a test for mocha hooks");
    });

    //will mark this test as pending
    it('Let\'s look at a pending test');
});

describe("index.js", () => {
    describe("sayHello function", () => {
        it("should say Hello all!", () => {

            const str = sayHello();
            expect(str).to.equal("Hello all!")
        })
    })
});

describe("Comparing objects and arrays", () => {
   it("can deep equal object properties", () => {
       const obj1 = {
           prop1: "test",
           prop2: 4
       }

       const obj2 = {
           prop1: "test",
           prop2: 4
       }
        
       expect(obj1).to.be.an('object').and.to.be.deep.equals(obj2); //chaining
       obj1.should.be.an('object').and.to.be.deep.equals(obj2); //using should
       assert.deepEqual(obj1, obj2);
   });

    it('can verify if an array includes a value', () => {
        let numbers = [1, 2, 3, 4];

        expect(numbers).to.be.an('array').that.includes(3);
        (numbers).should.be.an('array').that.includes(3);
        assert.isArray(numbers);
    });
});