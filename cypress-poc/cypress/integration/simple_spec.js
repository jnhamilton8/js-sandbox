describe('My First Test', () => {
    it('Visits the Kitchen sink, gets, types and asserts ', () => {
        cy.visit('https://example.cypress.io')

        //when you run the test, will open the Cypress debugger
       // cy.pause()

        //finds an element on the page that contains the text
        cy.contains('type').click()

        //assertion to check we are on the expected page
        cy.url().should('include', '/commands/actions')

        //get an input, type into it and verify the value has been updated
        //get selects elements based on a CSS class
        cy.get('.action-email')
            .type('fake@email.com')
            .should('have.value', 'fake@email.com')
    });
})


