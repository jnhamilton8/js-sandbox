class MyArray {
    constructor() {
        this.length = 0;
        this.data = {};
    }

    get(index) {
        return this.data[index];
    }

    push(item) {
        this.data[this.length] = item; //creates property of 0 = item in object
        this.length++;
        return this.length; //return the length of the array
    }

    pop() {
        const lastItem = this.data[this.length-1];
        delete this.data[this.length-1]; //delete removes property from an object, so can't type delete lastItem
        this.length--;
        console.log(lastItem);
        return lastItem
    }

    delete(index) {
        const item = this.data[index];
        this.shiftItems(index);
    }

    shiftItems(index) {
        //start from index we want to delete from, iterate until the end, take each item in data and
        //shift it to left by 1.
        for(let i = index; i < this.length-1; i++) {
            this.data[i] = this.data[i+1];
        }
        delete this.data[this.length-1] //delete the last item, as everything has shifted to left, leaves property at end
        this.length--;
    }
}

const newArray = new MyArray();
newArray.push('hi');
newArray.push('you');
newArray.pop();
newArray.push('!');
newArray.delete(1);
console.log(newArray);