class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class Queue {
    constructor() {
        this.first = null;
        this.last = null;
        this.length = 0;
    }

    peek() {
        return this.first;
    }

    //add to back of the queue
    enqueue(value) {
        const newNode = new Node(value);

        if (this.length === 0) {
            this.first = newNode;
            this.last = newNode;
        } else {
            this.last.next = newNode; //point the previous last node to the new last node which is newNode
            this.last = newNode; // set the last node as newNode
        }
        this.length++;
        return this;
    }

    //remove from front of the queue
    dequeue() {
        if (!this.first) {
            return null;
        }

        if (this.first === this.last) {
            this.last = null;
        }

        this.first = this.first.next; //set the new first to be the one behind the one we are dequeuing
        this.length--;
        return this;
    }
}

const myQueue = new Queue();
console.log(myQueue.peek());
console.log(myQueue.enqueue("Joy"));
console.log(myQueue.enqueue("Matt"));
console.log(myQueue.enqueue("Pavel"));
console.log(myQueue.enqueue("Samir"));

console.log(myQueue.dequeue()); //will remove first from the queue i.e. Joy

