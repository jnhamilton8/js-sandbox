let calculations = 0;

function fibonacci() {
   let cache = {};
   return function fib(n) {
       if (n in cache) {
           return cache[n];
       } else {
           if (n < 2) {
               return n //base condition
           } else {
               cache[n] = fib(n-1) + fib(n-2);
               console.log(cache);
               return cache[n];
           }
       }
   }
}

const fasterFib = fibonacci();
console.log(fasterFib(10));

// time complexity is O(n) as opposed to usual recursive way of O(2^n)
// Have increased space complexity to store the cache
