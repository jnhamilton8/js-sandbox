// 5! = 5*4*3*2*1

function findFactorialRecursive(number) {
    if(number === 1) {
        return number
    }

    return number * findFactorialRecursive(number - 1);
}

function findFactorialIterative(number) {
    let fact = 1;
    for(let i=2; i <= number; i++) {
        fact = fact * i;
    }

    return fact;
}

//console.log(findFactorialRecursive(5));
//console.log(findFactorialIterative(5));

// Given a number N return the index value of the Fibonacci sequence, where the sequence is:
// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144 ...
// So, let's say we have n of 4, this would be 3
// So to get this number we need to add n-1 and n-2, but we don't know what any of the numbers are so we need to do this
// for all of them.

function fibonacciRecursive(n) {
    if (n < 2) {
        return n; // return either 0 or 1, remember n is the index
    }

    return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
}

function fibonacciIterative(n) {
    let arr = [0, 1];  //set up our array with the first 2 numbers which will always be the same
    for (let i = 2; i <= n; i++) {
        arr.push(arr[i-2] + arr[i-1]); //sum previous 2 numbers in array to get current one (i) and push to array
    }

    return arr[n]; //return the value at the 5th index
}

//console.log(fibonacciRecursive(5));
//console.log(fibonacciIterative(5));

//Implement a function that reverses a string using iteration...and then recursion!
function reverseStringRecursive(str) {
    if (str === "") {
        return ""; //base case,
    } else {
        return reverseStringRecursive(str.substr(1)) + str.charAt(0);
    }
}

//console.log(reverseStringRecursive('hello'));
//should return: 'olleh'

/*
First Part of the recursion method

Each call: str === "?"        	                  reverseString(str.subst(1))     + str.charAt(0)
1st call – reverseString("Hello")   will return   reverseString("ello")           + "h"
2nd call – reverseString("ello")    will return   reverseString("llo")            + "e"
3rd call – reverseString("llo")     will return   reverseString("lo")             + "l"
4th call – reverseString("lo")      will return   reverseString("o")              + "l"
5th call – reverseString("o")       will return   reverseString("")               + "o"

Second part of the recursion method
The method hits the if condition and the most highly nested call returns immediately

5th call will return reverseString("") + "o" = "o"
4th call will return reverseString("o") + "l" = "o" + "l"
3rd call will return reverseString("lo") + "l" = "o" + "l" + "l"
2nd call will return reverserString("llo") + "e" = "o" + "l" + "l" + "e"
1st call will return reverserString("ello") + "h" = "o" + "l" + "l" + "e" + "h"
*/

function reverseString(str) {
    let reversed = '';
    for (let i = str.length - 1; i >= 0; i--) {
        reversed += str[i];
    }
    return reversed;
}

console.log(reverseString('hello'));