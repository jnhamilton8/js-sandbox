//using a linked list to create a stack

class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class Stack {
    constructor() {
        this.top = null;
        this.bottom = null;
        this.length = 0;
    }

    //see very top node
    peek() {
        return this.top;
    }

    //add node to top of stack
    push(value) {
        const newNode = new Node(value);

        if (this.length === 0) { //if the stack is empty then set the newNode to be the top and the bottom
            this.top = newNode;
            this.bottom = newNode;
        } else {
            const holdingPointer = this.top; //whatever is on top is going to get replaced by the newNode
            this.top = newNode; //set the new top node to be the newNode
            this.top.next = holdingPointer; //the old top will now be right after the new to
        }
        this.length++;
        return this;
    }

    //remove from top of stack
    pop() {
        if (!this.top) {  //if it's empty, return null
            return null;
        }

        if (this.top === this.bottom) { //if we remove the only node left we need to reset the bottom to null
            this.bottom = null;
        }

        this.top = this.top.next;
        this.length--;
        return this;
    }

    isEmpty() {
        return this.length === 0;
    }
}

const myStack = new Stack();
//console.log(myStack.peek());
myStack.push("google");
myStack.push("udemy");
myStack.push("discord");
//console.log(myStack);
//console.log(myStack.peek());
console.log(myStack.pop());
console.log(myStack);
console.log(myStack.isEmpty());