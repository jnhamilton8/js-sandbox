function reverse(str) {
    //check input
    if(!str || str.length < 2 || typeof str !== 'string') {
        return "Invalid parameter";
    }

    const backwardsArray = [];
    const totalChars = str.length-1; //how many chars in string
    for (let i = totalChars; i >= 0; i--){
        backwardsArray.push(str[i]);
    }
    return backwardsArray.join('');
}

function reverse2(str) {
    if(!str || str.length < 2 || typeof str !== 'string') {
        return "Invalid parameter";
    }

    return str.split('').reverse().join('');
}

const reverse3 = str => str.split('').reverse().join('');

const reverse4 = str => [...str].reverse().join('');

console.log(reverse("hello"));
console.log(reverse2("hello"));
console.log(reverse3("hello"));
console.log(reverse4("hello"));