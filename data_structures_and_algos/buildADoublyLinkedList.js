//10-->5-->16
//JS does not have linked lists

/*let myLinkedList = {
    head: {
        value: 10,        //value of node
        next: {
            value: 5,
            next: {
                value: 16,
                next: null  //pointer to next node
            }
        }
    }
};*/

class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
        this.previous = null;
    }
}

class DoublyLinkedList {
    constructor(value) {
        this.head = {
            value: value,
            next: null, //only one node when initiate
            previous: null
        };
        this.tail = this.head; //set the tail object to point to the head reference, so end node always references this.tail
        this.length = 1; //one item when create the linked list
    }

    //append to the end of the list
    append(value) {
        const newNode = new Node(value);

        //set previous of newly added node at end to the current tail
        newNode.previous = this.tail;
        //set CURRENT tail pointer to the new mode
        this.tail.next = newNode;
        //now set the tail reference to now be the new node
        this.tail = newNode;
        this.length++;

        return this;
    }

    //append to the beginning of the list
    prepend(value) {
        const newNode = new Node(value);

        newNode.next = this.head;
        this.head.previous = newNode; //as it's no longer the head, set reference to be the new node
        this.head = newNode; //now reset the head to be the newNode
        this.length++;

        return this.printList();
    }

    insert(index, value) {
        //check params
        if(index >= this.length || index === 0) {
            return this.append(value);
        }

        const newNode = new Node(value);
        //store the node that is just before the one you want to insert at index x
        const beforeNode = this.traverseToIndex(index-1);
        //store the pointer to the node the preceding node is pointing to
        const afterNode = beforeNode.next;

        //set the preceding node next to point to inserted node
        beforeNode.next = newNode;
        //point the newly inserted node previous to point to the one that was before it in the list
        newNode.previous = beforeNode;
        //set the previous pointer of the node after the newly inserted one to point to the newly inserted node
        afterNode.previous = newNode;
        //set the inserted node to point to node that is now after it in the list
        newNode.next = afterNode;
        this.length++;

        return this.printList();
    }

    remove(index) {
        //check params
        if(index === 0) { //if we want to remove the head
            const currentHead = this.head;
            this.head = currentHead.next;
            return this.printList()
        }

        if(index >= this.length) {
            return console.log("Invalid index");
        }
        //get the node before the one we want to remove
        const beforeNode = this.traverseToIndex(index-1);

        //get the node we want to delete, which is the pointer to next for the node preceding it
        const nodeToDelete = beforeNode.next;

        if(nodeToDelete.next) { //if we remove the tail, it won't have a next, so guard against this
            const afterNode = nodeToDelete.next; //get the node after the one we want to delete
            afterNode.previous = beforeNode; //And reset the previous node to point to the one before the one that was deleted!
        }

        //point the preceding node to the node that the deleted node was pointing to
        beforeNode.next = nodeToDelete.next;
        this.length--;

        return this.printList()
    }

    traverseToIndex(index) {
        let counter = 0;
        let currentNode = this.head; //start at the beginning of list

        while(counter !== index) { //loop through nodes until counter = index
            currentNode = currentNode.next; //move the current node on
            counter++;
        }
        return currentNode;
    }

    printList() {
        const array = [];
        let currentNode = this.head;
        while(currentNode !== null) {
            array.push(currentNode.value)
            currentNode = currentNode.next;
        }
        return array;
    }
}

const myLinkedList = new DoublyLinkedList(10) ; //give value to start off the list, in the head
myLinkedList.append(5);
myLinkedList.append(16);
myLinkedList.prepend(1);
myLinkedList.prepend(2);
myLinkedList.insert(1, 99);
myLinkedList.insert(20,88);
myLinkedList.remove(0);
console.log(myLinkedList.printList());
myLinkedList.remove(0);
console.log(myLinkedList);
console.log(myLinkedList.printList());
myLinkedList.remove(10);