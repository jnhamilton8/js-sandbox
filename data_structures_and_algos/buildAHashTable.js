class HashTable {
    constructor(size) {
        this.data = new Array(size); //construct an Array of the size required
    }

    // _ in JS means it is a private property - developer standard, don't access it, even though you can :)
    _hash(key) {
        let hash = 0;
        for (let i = 0; i < key.length; i++) {  //iterate over length of key
            //charCodeAt returns UTF-16 code of the letter
            //add character code to hash and multiply by index to keep it unique
            //Modulo this by length of the array
            hash = (hash + key.charCodeAt(i) * i) % this.data.length;
            console.log(hash);
        }
        return hash;
    }

    set(key, value) {
        let address = this._hash(key);
        if (!this.data[address]) { //if this address does not exist
            this.data[address] = []; //create an empty array at this location (this is to prevent collisions, we will add each item to this array (as in a list) at this bucket index, as below)
        }
        this.data[address].push([key, value]); //then go ahead and add the key and value to the end of the array created above
        console.log(this.data);
    }

    get(key) {
        let address = this._hash(key);  //get the bucket location
        const currentBucket = this.data[address]; //return the list of things at that bucket location
        if (currentBucket) { //if the bucket list has something in it i.e. it is not undefined
            for(let i = 0; i < currentBucket.length; i++) {
                //grab the first array, then grab first element of that array i.e. the key, see if it matches the key we are looking for
                if(currentBucket[i][0] === key) {
                    return currentBucket[i][1];  //return the value of the key we passed in
                }
            }
        }
        return undefined; //nothing in the bucket
    }

    keys() {
        const keysArray = [];
        for(let i = 0; i < this.data.length; i++) { //iterate over entire array
            if(this.data[i]) { //if there is something in that memory space
                keysArray.push(this.data[i][0][0]) //insert the key into keysArray - as the items are stored in an array, within an array, need to get them with [0] [0]
            }
        }
        return keysArray;
    }
}

const myHashTable = new HashTable(50);
myHashTable.set('grapes', 10000);
myHashTable.set('apples', 54);
myHashTable.set('oranges', 2);
console.log(myHashTable.get('grapes'));
console.log(myHashTable.keys());

