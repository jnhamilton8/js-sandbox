//using a linked list to create a stack

class Stack {
    constructor() {
        this.array = [];
    }

    //see very top node
    peek() {
        return this.array[this.array.length -1];
    }

    //add node to top of stack
    push(value) {
        this.array.push(value);
        return this;
    }

    //remove from top of stack
    pop() {
        this.array.pop();
        return this;
    }

    isEmpty() {
        return this.array.length === 0;
    }
}

const myStack = new Stack();
//console.log(myStack.peek());
myStack.push("google");
myStack.push("udemy");
myStack.push("discord");
//console.log(myStack);
console.log(myStack.peek());
myStack.pop();
console.log(myStack);
console.log(myStack.isEmpty());