//Google question - return first recurring character
//Given an array = [2,5,1,2,3,5,1,2,4]
//It should return 2

//Given an array = [2,1,1,2,3,5,1,2,4]
//It should return 1

//Given an array = [2,3,4,5]
//It should return undefined

//first approach
function firstRecurringCharacter(array) {
    const countOfNumbers = [];

    for(let i = 0; i < array.length; i++) {
        let itemInArray = array[i];
        if(countOfNumbers.includes(itemInArray)) {
            return "First recurring character is: " + itemInArray;
        } else {
            countOfNumbers.push(itemInArray);
        }
    }
    console.log(countOfNumbers);
}

function storeCountOfItemsInHashtable(array) {
    const countOfNumbers = new Map();
    const initialCount = 1;

    for(let i = 0; i < array.length; i++) {
        let itemInArray = array[i];
        if(countOfNumbers.has(itemInArray)) {
            let currentItemCount = countOfNumbers.get(itemInArray);
            currentItemCount++;
            if(currentItemCount > 1) {
                return "First recurring character is: " + itemInArray;
            }
            countOfNumbers.set(itemInArray, currentItemCount);
        } else {
            countOfNumbers.set(itemInArray, initialCount);
        }
    }
    return undefined;
}

function courseFirstRecurringCharacter(array) {
    let map = {};
    for(let i = 0; i < array.length; i++) {
        let currentItem = array[i];
       if(map[currentItem] !== undefined) { //if key already exists
           return currentItem; //just return it
       } else
           map[currentItem] = i; //add the key to the map, note value can be anything, doesn't matter
    }
    console.log(map);
    return undefined; //if no duplicates, return undefined

}


//console.log(firstRecurringCharacter([2,5,1,2,3,5,1,2,4]));
//console.log(firstRecurringCharacter([2,1,1,2,3,5,1,2,4]));
//console.log(firstRecurringCharacter([2,3,4,5]));
//console.log(storeCountOfItemsInHashtable([2,5,1,2,3,5,1,2,4]));
//console.log(storeCountOfItemsInHashtable([2,1,1,2,3,5,1,2,4]));
//console.log(storeCountOfItemsInHashtable([2,3,4,5]));
console.log(courseFirstRecurringCharacter([2,5,1,2,3,5,1,2,4]));
console.log(courseFirstRecurringCharacter([2,1,1,2,3,5,1,2,4]));
console.log(courseFirstRecurringCharacter([2,3,4,5]));