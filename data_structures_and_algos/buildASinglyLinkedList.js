//10-->5-->16
//JS does not have linked lists

/*let myLinkedList = {
    head: {
        value: 10,        //value of node
        next: {
            value: 5,
            next: {
                value: 16,
                next: null  //pointer to next node
            }
        }
    }
};*/

class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}

class LinkedList {
    constructor(value) {
        this.head = {
            value: value,
            next: null //only one node when initiate
        };
        this.tail = this.head; //set the tail object to point to the head reference, so end node always references this.tail
        this.length = 1; //one item when create the linked list
    }

    //my attempt!
    appendJH(value) {
        //set up the node to insert
        const newNode = new Node(value);

        // First, update the current tail pointer to point to the new node
        if(this.head.next === null) { // If we only have the head, then update the head next node and set new tail node
            this.head.next = newNode;
            this.tail = newNode;
        } else { //otherwise, update the CURRENT last node (tail) next to be the inserted node, as was previously null
            this.tail.next = newNode;
        }

        //Now, let's update the tail node
        //We have to find it, so transverse from the beginning, head, to end
        this.tail = this.head;
        while(this.tail.next !== null) { //while we still have a next node
              this.tail = this.tail.next; //set the tail to be the next node in line
        }

        this.tail = newNode; //we have the tail! Set it to be the newNode
        this.length++; //And finally, increase the length

        return this;
    }

    //better!
    append(value) {
        const newNode = new Node(value);

        //set CURRENT tail pointer to the new mode
        this.tail.next = newNode;
        //now set the tail reference to now be the new node
        this.tail = newNode;
        this.length++;

        return this
    }

    //my attempt
    prepend(value) {
        const newNode = new Node(value);

        newNode.next = this.head;
        this.head = newNode;
        this.length++;

        return this;
    }

    insert(index, value) {
        //check params
        if(index >= this.length || index === 0) {
            return this.append(value);
        }

        const newNode = new Node(value);
        //store the node that is just before the one you want to insert at index x
        const precedingNode = this.traverseToIndex(index-1);
        //store the pointer to the node the preceding nodes is pointing to
        const holdingPointerToNextForPrecedingNode = precedingNode.next;
        //set the preceding node to point to inserted node
        precedingNode.next = newNode;
        //set the inserted node to point to the same node as the preceding nodes was pointing to
        newNode.next = holdingPointerToNextForPrecedingNode;
        this.length++;

        return this.printList();
    }

    remove(index) {
        //check params
        if(index === 0) { //if we want to remove the head
            const currentHead = this.head;
            this.head = currentHead.next;
            return this.printList()
        }

        if(index >= this.length) {
            return console.log("Invalid index");
        }
        //get the node before the one we want to remove
        const precedingNode = this.traverseToIndex(index-1);
        //get the node we want to delete, which is the pointer to next for the node before it
        const nodeToDelete = precedingNode.next;
        //point the preceding node to the node that the deleted node was pointing to
        precedingNode.next = nodeToDelete.next;
        this.length--;

        return this.printList()
    }

    reverse() {
        //if list only contains one item, return the node
        if(!this.head.next) {
            return this.head;
        }

        let first = this.head; //get first node
        this.tail = this.head; //point tail to be the current head
        let second = first.next; //get second node

        while(second) { //as long as second node is not null
            const temp = second.next; //create temp variable for third item
            second.next = first; //second node now points to the first node
            first = second; // set the first node i.e. the head to be the second
            second = temp; //set the second node to be the third node in list
            //then loop again
            //so we have pointed second to first item, moved on to third item, which we will point to second item in list
        }
        this.tail.next = null; //set the new tail next to be null
        this.head = first; //by the time we loop through it all, first is now the node at end of list

        return this;
    }

    traverseToIndex(index) {
        let counter = 0;
        let currentNode = this.head; //start at the beginning of list

        while(counter !== index) { //loop through nodes until counter = index
            currentNode = currentNode.next; //move the current node on
            counter++;
        }
        return currentNode;
    }

    printList() {
        const array = [];
        let currentNode = this.head;
        while(currentNode !== null) {
            array.push(currentNode.value)
            currentNode = currentNode.next;
        }
        return array;
    }
}

const myLinkedList = new LinkedList(10) ; //give value to start off the list, in the head
myLinkedList.append(5);
myLinkedList.append(16);
//myLinkedList.prepend(1);
//myLinkedList.prepend(2);
myLinkedList.insert(2, 99);
myLinkedList.insert(20,88);
//myLinkedList.remove(2);
//myLinkedList.remove(0);
console.log(myLinkedList.printList());
//myLinkedList.remove(10);
console.log(myLinkedList.reverse());